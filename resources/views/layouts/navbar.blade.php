<div class="container">
    <div class="bg-black text-center text-white py-5">
        <h1>Complimentary Free Shipping On Orders Over $50</h1>
    </div>
    <div class="navbar bg-white">
        <div class="navbar-start px-6">
            {{-- <div class="dropdown">
                <div tabindex="0" role="button" class="btn btn-ghost lg:hidden">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24"
                        stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                            d="M4 6h16M4 12h8m-8 6h16" />
                    </svg>
                </div>

            </div> --}}
            <a href="" class="text-xl text-black text-[20px] font-bold">MENU</a>
        </div>
        <div class="navbar-center text-center text-black">
            <ul>
                <li>Asixth</li>
                <li>COFFEE COMPANY</li>
            </ul>
        </div>
        <div class="navbar-end lg:flex text-2xl text-black text-[20px] font-bold">
            <ul class="menu menu-horizontal px-5">
                <li class="px-5">
                    <a href="">ACCOUNT</a>
                </li>
                <li>
                    <a href="">CART (0)</a>
                </li>
            </ul>
        </div>
    </div>
</div>
