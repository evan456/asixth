<footer class="pt-5">
    <div class="container">
        <div class="bg-[#292D32] text-white">
            <div class="flex py-20 px-10">
                <div class="flex-auto w-1/3 pr-44"> {{-- jarak responsive (revisi) --}}
                    Subscribe to
                    <div class="py-5">
                        <hr>
                    </div>

                    <input type="text" placeholder="Email Address" class="input input-bordered w-full border-2 border-white focus:outline-white" />

                    <div class="pt-8 flex">
                        <div class="flex">
                            <input type="checkbox" class="checkbox checkbox-lg border-white border-2" />
                        </div>
                        <div class="flex px-2 text-[14px]">
                            <h1>Subscribe to recieve communications from ASIXTH COFFEE <br>
                                about our products and services. By subscribing, you <br>
                                confirm you have read and accept our privacy policy</h1>
                        </div>

                    </div>
                </div>
                <div class="flex-auto w-">
                    <ul>
                        <li>Orders and Support</li>

                        <div class="pt-5">
                            <hr class="w-[250px]">
                        </div>
                        <div class="pt-5">
                            <li class="pt-3">Track Order </li>
                            <li class="pt-3">Shipping</li>
                            <li class="pt-3">FAQs</li>
                        </div>
                    </ul>
                </div>
                <div class="flex-auto w-32">
                    <ul>
                        <li>Services</li>
                        <div class="pt-5">
                            <hr class="w-[201px]">
                        </div>
                        <div class="pt-5">
                            <li class="pt-3">Typology </li>
                            <li class="pt-3">Wholesale</li>
                            <li class="pt-3">Consultations</li>
                        </div>
                    </ul>
                </div>
                <div class="flex-auto w-32">
                    <ul>
                        <li>Enquires</li>
                        <div class="pt-5">
                            <hr class="w-[181px]">
                        </div>
                        <div class="pt-5">
                            <li class="pt-3">Press </li>
                            <li class="pt-3">Autonomy</li>
                            <li class="pt-3">Quick Chat</li>
                        </div>
                    </ul>
                </div>
            </div>
            <div class="flex py-20 px-10">
                <div class="flex-auto w-128">
                    <ul>
                        <li>Our Commitment to Earth</li>
                        <div class="pt-5">
                            <hr class="w-[400px]">
                        </div>
                        <div class="pt-5 text-[13px]">
                            <li class="pt-3">ALL ASIXTH COFFEE products and packaging are sustainably made. </li>
                            <li class="pt-3">Our boxs and bags made from recycled materials </li>
                        </div>
                    </ul>
                </div>
                <div class="flex-auto w-32">
                    <ul>
                        <li>About</li>

                        <div class="pt-5">
                            <hr class="w-[250px]">
                        </div>
                        <div class="pt-5">
                            <li class="pt-3">Terms and Conditions </li>
                            <li class="pt-3">Careers</li>
                            <li class="pt-3">Our Story</li>
                            <li class="pt-3">Cookies Policy</li>
                        </div>
                    </ul>
                </div>
                <div class="flex-auto w-32">
                    <ul>
                        <li>Initiatives</li>
                        <div class="pt-5">
                            <hr class="w-[201px]">
                        </div>
                        <div class="pt-5">
                            <li class="pt-3">Editions </li>
                            <li class="pt-3">Guides</li>
                            <li class="pt-3">Faces</li>
                            <li class="pt-3">Locals</li>
                        </div>
                    </ul>
                </div>
                <div class="flex-auto w-32">
                    <ul>
                        <li>Social Media</li>
                        <div class="pt-5">
                            <hr class="w-[181px]">
                        </div>
                        <div class="pt-5">
                            <li class="pt-3">Instagram </li>
                            <li class="pt-3">Twitter / X</li>
                            <li class="pt-3">LinkedIn</li>
                        </div>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</footer>
