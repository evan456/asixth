<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    @vite(['resources/css/app.css', 'resources/scss/app.scss', 'resources/js/app.js'])
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />

</head>

<body>
    @include('layouts.navbar')

    {{-- <div class="container">
        <div class="h-96 carousel carousel-vertical items-center flex w-full">
            <div class="carousel-item h-full">
              <img src="{{asset('images/alex.png')}}" />
            </div>
            <div class="carousel-item h-full">
              <img src="https://img.daisyui.com/images/stock/photo-1565098772267-60af42b81ef2.jpg" />
            </div>
            <div class="carousel-item h-full">
              <img src="https://img.daisyui.com/images/stock/photo-1572635148818-ef6fd45eb394.jpg" />
            </div>
            <div class="carousel-item h-full">
              <img src="https://img.daisyui.com/images/stock/photo-1494253109108-2e30c049369b.jpg" />
            </div>
            <div class="carousel-item h-full">
              <img src="https://img.daisyui.com/images/stock/photo-1550258987-190a2d41a8ba.jpg" />
            </div>
            <div class="carousel-item h-full">
              <img src="https://img.daisyui.com/images/stock/photo-1559181567-c3190ca9959b.jpg" />
            </div>
            <div class="carousel-item h-full">
              <img src="https://img.daisyui.com/images/stock/photo-1601004890684-d8cbf643f5f2.jpg" />
            </div>
          </div>
    </div> --}}
    <div class="container">
        <div class="your-class relative">
            <div class="carousel-item w-full h-screen relative">
                <img class="relative mx-auto konten" src="{{ asset('images/alex.png') }}" />
                <div class="backcrop-blur-xl bg-black/30 absolute top-0 h-full overlay" style="display: none">
                    <h2>Photographer & Motion Stop Cinematographer</h2>
                    <h2 class="py-5">Alex- Los Angeles, California </h2>
                    <h2>To me, a good cup of coffee signifies more than just a beverage; it’s a ritual. It’s about
                        savoring each sip, indulging in the rich aroma, and appreciating the craftsmanship behind every
                        brew. It’s a moment of blissful solitude amidst the chaos of everyday life
                    </h2>
                    <h2 class="pt-10">Trying ASIXTH coffee has revolutionized my coffee experience. Its distinct flavor
                        profile and
                        exquisite aroma have elevated my standards for what a truly exceptional cup of coffee should be.
                    </h2>
                </div>
            </div>
            <div class="carousel-item w-full h-screen relative">
                <img class="relative mx-auto konten" src="{{ asset('images/alex.png') }}" />
                <div class="backcrop-blur-xl bg-black/30 absolute top-0 h-full overlay" style="display: none">
                    <h2>Photographer & Motion Stop Cinematographer</h2>
                    <h2 class="py-5">Alex- Los Angeles, California </h2>
                    <h2>To me, a good cup of coffee signifies more than just a beverage; it’s a ritual. It’s about
                        savoring each sip, indulging in the rich aroma, and appreciating the craftsmanship behind every
                        brew. It’s a moment of blissful solitude amidst the chaos of everyday life
                    </h2>
                    <h2 class="pt-10">Trying ASIXTH coffee has revolutionized my coffee experience. Its distinct flavor
                        profile and
                        exquisite aroma have elevated my standards for what a truly exceptional cup of coffee should be.
                    </h2>
                </div>
            </div>
            <div class="carousel-item w-full h-screen relative">
                <img class="relative mx-auto konten" src="{{ asset('images/alex.png') }}" />
                <div class="backcrop-blur-xl bg-black/30 absolute top-0 h-full overlay" style="display: none">
                    <h2>Photographer & Motion Stop Cinematographer</h2>
                    <h2 class="py-5">Alex- Los Angeles, California </h2>
                    <h2>To me, a good cup of coffee signifies more than just a beverage; it’s a ritual. It’s about
                        savoring each sip, indulging in the rich aroma, and appreciating the craftsmanship behind every
                        brew. It’s a moment of blissful solitude amidst the chaos of everyday life
                    </h2>
                    <h2 class="pt-10">Trying ASIXTH coffee has revolutionized my coffee experience. Its distinct flavor
                        profile and
                        exquisite aroma have elevated my standards for what a truly exceptional cup of coffee should be.
                    </h2>
                </div>
            </div>
            <div class="carousel-item w-full h-screen relative">
                <img class="relative mx-auto konten" src="{{ asset('images/alex.png') }}" />
                <div class="backcrop-blur-xl bg-black/30 absolute top-0 h-full overlay" style="display: none">
                    <h2>Photographer & Motion Stop Cinematographer</h2>
                    <h2 class="py-5">Alex- Los Angeles, California </h2>
                    <h2>To me, a good cup of coffee signifies more than just a beverage; it’s a ritual. It’s about
                        savoring each sip, indulging in the rich aroma, and appreciating the craftsmanship behind every
                        brew. It’s a moment of blissful solitude amidst the chaos of everyday life
                    </h2>
                    <h2 class="pt-10">Trying ASIXTH coffee has revolutionized my coffee experience. Its distinct flavor
                        profile and
                        exquisite aroma have elevated my standards for what a truly exceptional cup of coffee should be.
                    </h2>
                </div>
            </div>

            {{-- <div>your content</div>
            <div>your content</div> --}}

        </div>
    </div>

    @include('layouts.footer')



    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>


    <script>
        $(document).ready(function() {
            $(".konten").click(function() {
                $(".overlay").slideDown("slow");
            });
            $(".overlay").click(function() {
                $(".overlay").slideUp("slow");
            });
            const slider = $('.your-class').slick({
                dots: true,
                // adaptiveHeight: true,
                swipeToSlide: true,
                vertical: true,
                prevArrow: false,
                nextArrow: false,
                infinite: true,
                // rtl: true,
                customPaging: function(slider, i) {
                    return `<div class='h-3 w-3 bg-white rounded-full my-2 aktif'></div>`
                }

            });

            slider.on('wheel', (function(e) {
                e.preventDefault();

                if (e.originalEvent.deltaY < 0) {
                    $(this).slick('slickNext');
                } else {
                    $(this).slick('slickPrev');
                }
            }));

        });
    </script>
</body>

</html>
